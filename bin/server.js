var net = require('net');
var ip = require('ip');

var port = '3000';

var socket_created = { socket: undefined, status: undefined };

var server = net.createServer(function (socket) {
    console.log("Conectado")
    socket_created['socket'] = socket;

    socket.on('data', function (data) {
        socket_created['status'] = String(data);        
    });

    socket.on('close', function (hadError) {
        if (socket_created['socket'] != undefined) {
            console.log("Desconectado");
            socket_created = {};
        }
    });
});

server.listen(port, ip.address);

module.exports = {
    socket_data: socket_created,
    server: server
};