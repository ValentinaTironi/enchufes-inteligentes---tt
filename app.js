var server = require('./bin/server'); //importa el server (conexion con rpi)
var path = require('path'); //to working with files and dir paths
var express = require('express'); //framework to node js web applications
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'views')));

app.get('/', function(req, res) {
    var rpi_data = server.socket_data;
    res.render('index', { rpi_data: rpi_data, status: rpi_data['status'] });
});

app.post('/', function(req, res) {
    var accion = req.body.accion;
    try {
        server.socket_data.socket.write(accion);
    } catch (error) {
        console.log(error.message)
    }
    res.redirect('/');
})

app.listen(5000);
